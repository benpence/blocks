import * as immutable from "immutable"

export enum Color {
  Red = "Red",
  Yellow = "Yellow",
  White = "White",
  Green = "Green",
  Blue = "Blue",
  Black = "Black",
}

export interface Brick {
  readonly cornerPos: [number, number, number]
  readonly width: number
  readonly length: number
  readonly color: Color
  readonly rotationsOf90?: number
}

export interface Landscape {
  readonly bricks: immutable.List<Brick>
}