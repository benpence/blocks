import * as babylon from "babylonjs"
import * as immutable from "immutable"
import * as types from "blocks/client/types"
import * as util from "blocks/util"

const colors = immutable.List.of<[types.Color, babylon.Color4]>(
  [types.Color.Red, new babylon.Color4(1, 0, 0)],
  [types.Color.Yellow, new babylon.Color4(1, 1, 0)],
  [types.Color.White, new babylon.Color4(1, 1, 1)],
  [types.Color.Green, new babylon.Color4(0, 1, 0)],
  [types.Color.Blue, new babylon.Color4(0, 0, 1)],
  [types.Color.Black, new babylon.Color4(0.2, 0.2, 0.2)]
)

export interface Handler {
  readonly render: (engine: babylon.Engine, canvas: HTMLCanvasElement) => babylon.Scene
  // TODO: Replace w/ onEvent and Event type
  readonly onMouseMove?: (scene: babylon.Scene) => Handler
  readonly onClick?: (scene: babylon.Scene) => Handler
  readonly onKey?: (scene: babylon.Scene, key: string) => Handler
}

export class LandscapeHandler implements Handler {
  readonly landscape: types.Landscape
  readonly onChange: (newLandscape: types.Landscape) => void
  readonly addBrick: types.Brick

  static Ground: types.Brick = { cornerPos: [0, -1, 0], width: 30, length: 30, color: types.Color.Green }
  static AddBrick: types.Brick = { cornerPos: util.t3(0, 0, 0), width: 4, length: 2, color: types.Color.Red }

  static CameraName = "ground"
  static GroundName = "ground"
  static LightName = "light"
  static AddBrickName = "add"
  static BrickMaterialName = "brick-material"

  static EdgeLength = 1
  static BetaMin = 0
  static BetaMax = Math.PI / 2
  static RadiusMin = 30 * LandscapeHandler.EdgeLength
  static RadiusMax = 50 * LandscapeHandler.EdgeLength

  constructor(
    landscape: types.Landscape,
    onChange: (landscape: types.Landscape) => void,
    addBrick: types.Brick = LandscapeHandler.AddBrick
  ) {
    this.landscape = landscape
    this.onChange = onChange
    this.addBrick = addBrick
  }

  public render = (engine: babylon.Engine, canvas: HTMLCanvasElement): babylon.Scene => {
    const scene = new babylon.Scene(engine)

    new babylon.HemisphericLight(LandscapeHandler.LightName, new babylon.Vector3(0, 1, 0), scene)

    const brickMaterial = new babylon.StandardMaterial(
      LandscapeHandler.BrickMaterialName,
      scene
    )
    brickMaterial.specularColor = new babylon.Color3(0, 0, 0)

    const groundMesh = LandscapeHandler.brickMesh(
      LandscapeHandler.Ground,
      scene,
      LandscapeHandler.GroundName
    )

    this.replaceAddBrickMesh(this.addBrick, scene)

    this.landscape.bricks.forEach(b => LandscapeHandler.brickMesh(b, scene))

    const camera = new babylon.ArcRotateCamera(
      LandscapeHandler.CameraName,
      0,
      util.avg(LandscapeHandler.BetaMin, LandscapeHandler.BetaMax),
      util.avg(LandscapeHandler.RadiusMin, LandscapeHandler.RadiusMax),
      new babylon.Vector3(...LandscapeHandler.Ground.cornerPos),
      scene
    )

    camera.attachControl(canvas, true, true, 0)

    // Constrict rotation
    scene.registerBeforeRender(() => {
      camera.beta = Math.max(camera.beta, LandscapeHandler.BetaMin)
      camera.beta = Math.min(camera.beta, LandscapeHandler.BetaMax)
      camera.radius = Math.max(camera.radius, LandscapeHandler.RadiusMin)
      camera.radius = Math.min(camera.radius, LandscapeHandler.RadiusMax)
    })
    camera.setTarget(groundMesh)

    return scene
  }

  public onMouseMove = (scene: babylon.Scene): Handler => {
    const pick = LandscapeHandler.pickSquare(scene)

    if (pick !== null) {
      const addBrickMesh = scene.getMeshByName(LandscapeHandler.AddBrickName)
      const addCornerDelta = LandscapeHandler.cornerToCenter(addBrickMesh)

      addBrickMesh.setAbsolutePosition(pick.add(new babylon.Vector3(0, addCornerDelta.y, 0)))

      const newCorner = addBrickMesh.getAbsolutePosition()
        .subtract(LandscapeHandler.cornerToCenter(addBrickMesh))
      const newCornerPos = util.t3(
        newCorner.x / LandscapeHandler.EdgeLength,
        newCorner.y / LandscapeHandler.EdgeLength,
        newCorner.z / LandscapeHandler.EdgeLength,
      )

      const newAddBrick = { ...this.addBrick, cornerPos: newCornerPos }
      return new LandscapeHandler(this.landscape, this.onChange, newAddBrick)
    }

    return this
  }

  public onClick = (scene: babylon.Scene): Handler => {
    LandscapeHandler.brickMesh(this.addBrick, scene)
    const newLandscape = { bricks: this.landscape.bricks.push(this.addBrick) }

    this.onChange(newLandscape)
    return new LandscapeHandler(newLandscape, this.onChange, this.addBrick)
  }

  public onKey = (scene: babylon.Scene, key: string): Handler => {
    let newAddBrick: types.Brick

    switch (key) {

      // Rotate add brick
      case " ":
        const rotations = this.addBrick.rotationsOf90 !== undefined ? this.addBrick.rotationsOf90 : 0
        newAddBrick = { ...this.addBrick, rotationsOf90: (rotations + 1) % 4 }
        this.replaceAddBrickMesh(newAddBrick, scene)
        return new LandscapeHandler(this.landscape, this.onChange, newAddBrick)

      // Rotate colors
      case "Enter":
        const colorIdx = colors.findIndex(c => c[0] === this.addBrick.color)
        const newColor = colors.get((colorIdx + 1) % colors.size)[0]
        newAddBrick = { ...this.addBrick, color: newColor }

        this.replaceAddBrickMesh(newAddBrick, scene)
        return new LandscapeHandler(this.landscape, this.onChange, newAddBrick)

      // Remove most recently placed brick
      case "Backspace":
        const mostRecentBrick = this.landscape.bricks.last(undefined)

        if (mostRecentBrick === undefined) {
          return this
        }

        const mostRecentMesh = scene.getMeshByName(LandscapeHandler.brickName(mostRecentBrick))
        mostRecentMesh.dispose()

        const newLandscape = { bricks: this.landscape.bricks.pop() }
        this.onChange(newLandscape)
        return new LandscapeHandler(newLandscape, this.onChange, this.addBrick)

      default: return this
    }
  }

  replaceAddBrickMesh = (brick: types.Brick, scene: babylon.Scene): void => {
    const addBrickMesh = scene.getMeshByName(LandscapeHandler.AddBrickName)

    if (addBrickMesh !== null) {
      scene.removeMesh(addBrickMesh, true)
      addBrickMesh.dispose()
    }

    const newAddMesh = LandscapeHandler.brickMesh(
      brick,
      scene,
      LandscapeHandler.AddBrickName
    )
    newAddMesh.isPickable = false
  }

  static brickName = (brick: types.Brick): string => {
    const rotations = brick.rotationsOf90 !== undefined ? brick.rotationsOf90 : 0
    return `brick${brick.length}x${brick.width}@${brick.cornerPos}r${rotations}`
  }

  static connectorName = (brick: types.Brick, i: number, j: number): string => {
    return `${LandscapeHandler.brickName(brick)}-${i}x${j}`
  }

  // TODO: Would using Mesh#createInstance(...) improve performance?
  static brickMesh = (brick: types.Brick, scene: babylon.Scene, name?: string): babylon.AbstractMesh => {
    const color = colors.find(c => c[0] === brick.color)[1]
    const width = LandscapeHandler.EdgeLength * brick.length
    const height = LandscapeHandler.EdgeLength
    const depth = LandscapeHandler.EdgeLength * brick.width
    const rotations = brick.rotationsOf90 !== undefined ? brick.rotationsOf90 : 0
    const faceColors = new Array(6).fill(color)

    const mesh = babylon.MeshBuilder.CreateBox(
      name !== undefined ? name : LandscapeHandler.brickName(brick),
      { width, height, depth, faceColors }
    )

    const corner = new babylon.Vector3(...brick.cornerPos)
      .scale(LandscapeHandler.EdgeLength)
    const centerDelta = LandscapeHandler.cornerToCenter(mesh)

    const brickMaterial = scene.getMaterialByName(LandscapeHandler.BrickMaterialName)

    // Create connectors on top
    const size = LandscapeHandler.EdgeLength / 2
    const connectorBase = babylon.MeshBuilder.CreateCylinder(
      LandscapeHandler.connectorName(brick, -1, -1),
      { diameter: size, height: size, faceColors },
      scene
    )
    connectorBase.material = brickMaterial

    // Hide connector base mesh
    connectorBase.setEnabled(false)

    for (let i = 0; i < brick.width; i++) {
      for (let j = 0; j < brick.length; j++) {
        const connector = connectorBase.createInstance(LandscapeHandler.connectorName(brick, i, j))

        // Offset i x j edges + a bit extra to center it
        const connectorCenter = new babylon.Vector3(
          j * LandscapeHandler.EdgeLength + LandscapeHandler.EdgeLength / 2,
          // TODO: The connector intersects the brick; perhaps make it smaller and move it up?
          height - (height / 8),
          i * LandscapeHandler.EdgeLength + LandscapeHandler.EdgeLength / 2,
        )

        connector.isPickable = false
        connector.setAbsolutePosition(connectorCenter.subtract(centerDelta))
        mesh.addChild(connector)
      }
    }

    mesh.material = brickMaterial

    // Edges
    mesh.enableEdgesRendering()
    mesh.edgesWidth = 5;
    mesh.edgesColor = new babylon.Color4(0, 0, 0, 0.2)

    // Apply rotation to brick before placing it
    mesh.rotate(babylon.Axis.Y, rotations * Math.PI / 2)
    mesh.setAbsolutePosition(corner.add(centerDelta))

    scene.addMesh(mesh)
    return mesh
  }

  static cornerToCenter = (mesh: babylon.AbstractMesh): babylon.Vector3 => {
    return mesh.getBoundingInfo().minimum.multiplyByFloats(-1, -1, -1)
  }

  static pickSquare = (scene: babylon.Scene): babylon.Vector3 | null => {
    const pick = scene.pick(scene.pointerX, scene.pointerY)

    if (pick.pickedMesh !== null) {
      const pickSquare = new babylon.Vector3(
        Math.abs(Math.round(pick.pickedPoint.x / LandscapeHandler.EdgeLength)),
        Math.abs(Math.round(pick.pickedPoint.y / LandscapeHandler.EdgeLength)),
        Math.abs(Math.round(pick.pickedPoint.z / LandscapeHandler.EdgeLength)),
      ).scale(LandscapeHandler.EdgeLength)

      return pickSquare
    }

    return null
  }
}