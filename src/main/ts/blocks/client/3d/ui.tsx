import * as React from "react"
import * as babylon from "babylonjs"
import * as handler from "blocks/client/3d/handler"

export type SceneProps = {
  engineOptions?: babylon.EngineOptions,
  adaptToDeviceRatio?: boolean,
  sceneHandler: handler.Handler,
  width?: number | string,
  height?: number | string
}

export class Scene extends React.Component<SceneProps, handler.Handler> {
  public constructor(props: SceneProps) {
    super(props)
    this.state = props.sceneHandler
  }

  public shouldComponentUpdate = (nextProps: SceneProps, nextState: handler.Handler): boolean => {
    return false
  }

  public render = () => {
    const onCanvasLoaded = (canvas: HTMLCanvasElement) => {
      if (canvas !== null) {
        const engine = new babylon.Engine(
          canvas,
          true,
          this.props.engineOptions,
          this.props.adaptToDeviceRatio
        )

        const scene = this.state.render(engine, canvas)

        canvas.onmousemove = () => {
          if (this.state.onMouseMove !== undefined) {
            this.setState(this.state.onMouseMove(scene))
          }
        }

        canvas.onclick = () => {
          if (this.state.onClick !== undefined) {
            this.setState(this.state.onClick(scene))
          }
        }

        canvas.onkeydown = (event: KeyboardEvent): void => {
          if (this.state.onKey !== undefined) {
            this.setState(this.state.onKey(scene, event.key))
          }
        }

        // Disable right-click context menu
        canvas.oncontextmenu = () => false

        engine.runRenderLoop(() => scene.render())

        // Keyboard events won't require clicking on canvas
        canvas.focus()
      }
    }

    return <canvas
      className="scene"
      width={this.props.width}
      height={this.props.height}
      ref={onCanvasLoaded}
    />
  }
}