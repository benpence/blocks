import * as React from "react"
import * as ReactDOM from "react-dom"
import * as immutable from "immutable"
import * as handler from "blocks/client/3d/handler"
import * as ui from "blocks/client/3d/ui"
import * as util from "blocks/util"
import * as types from "blocks/client/types"

const Controls = () => {
  return (
    <div className="controls">
      <p className="title">Controls</p>
      <ul>
        <li>
          <span className="mouse place"><img src="static/img/mouse_left_click.png" /></span>
          <span className="controls-explanation">Place a brick at the cursor</span>
        </li>
        <li>
          <span className="mouse rotate-view"><img src="static/img/mouse_right_drag.png" /></span>
          <span className="controls-explanation">Rotate the view</span>
        </li>
        <li>
          <span className="mouse rotate-view"><img src="static/img/mouse_middle_click.png" /></span>
          <span className="controls-explanation">Zoom the view</span>
        </li>
        <li>
          <span className="key color-brick">Enter</span>
          <span className="controls-explanation">Change the color of the cursor brick</span>
        </li>
        <li>
          <span className="key rotate-brick">Space</span>
          <span className="controls-explanation">Rotate the cursor brick</span>
        </li>
        <li>
          <span className="key undo">Backspace</span>
          <span className="controls-explanation">Erase the most recently added brick</span>
        </li>
      </ul>
    </div>
  )
}

const BricksList = (props: { bricks: immutable.List<types.Brick> }) => {
  let groupedBricks = immutable.Map<util.Tuple3<number, number, types.Color>, number>()

  for (let brick of props.bricks) {
    const key = new util.Tuple3(brick.width, brick.length, brick.color)
    groupedBricks = groupedBricks.update(key, 0, n => n + 1)
  }

  return (
    <div className="bricks-list">
      <p className="title">Pieces</p>
      <ol>
        {groupedBricks.toSeq()
          .map((bricks, tuple) =>
            <li>{`${bricks} ${tuple.c} ${tuple.a}x${tuple.b}`}</li>
          )
          .toList()
        }
      </ol>
    </div>
  )
}

const Status = (props: { landscape: types.Landscape }) => {
  return (
    <div className="status">
      <Controls />
      <BricksList bricks={props.landscape.bricks} />
    </div>
  )
}

const App = (props: {}) => {
  const [landscape, setLandscape] = React.useState({ bricks: immutable.List() })
  const sceneHandler = new handler.LandscapeHandler(landscape, setLandscape)

  return (
    <div id="app">
      <h1>Blocks (<a href="https://gitlab.com/benpence/blocks">source</a>)</h1>
      <div id="content">
        <ui.Scene sceneHandler={sceneHandler} />
        <Status landscape={landscape} />
      </div>
    </div>
  )
}

ReactDOM.render(<App />, document.getElementById("root"))