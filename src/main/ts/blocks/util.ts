export const t3 = <A, B, C>(a: A, b: B, c: C): [A, B, C] => [a, b, c]

export const avg = (a: number, b: number): number =>
  a < b
    ? a + (b - a) / 2
    : b + (a - b) / 2

export class Tuple3<A, B, C> {
  public readonly a: A
  public readonly b: B
  public readonly c: C

  constructor(a: A, b: B, c: C) {
    this.a = a
    this.b = b
    this.c = c
  }

  public hashCode = (): number => {
    const s = `${this.a}${this.b}${this.c}`

    let number = 0
    for (let i = 0; i < s.length; i++) {
      number = number * 10 + s.charCodeAt(i)
    }

    return number
  }

  public equals = (other: any): boolean => {
    if (other === null || other === undefined) {
      return false
    }

    const otherTuple = other as Tuple3<A, B, C>

    return this.a === otherTuple.a && this.b === otherTuple.b && this.c === otherTuple.c
  }
}